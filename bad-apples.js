var replacements = {
	"ex-cop": "bad apple",
	"Ex-cop": "Bad apple",
	"excop": "bad apple",
	"Excop": "Bad apple",
	"ex-officer": "bad apple",
	"Ex-officer": "Bad apple",
	"exofficer": "bad apple",
	"Exofficer": "Bad apple",
	"police officer": "bad apple",
	"Police officer": "Bad apple",
	"policeofficer": "bad apple",
	"Policeofficer": "Bad apple",
	"police person": "bad apple",
	"Police person": "Bad apple",
	"policeperson": "bad apple",
	"Policeperson": "Bad apple",
	"policeman": "bad apple",
	"Policeman": "Bad apple",
	"police man": "bad apple",
	"Police man": "Bad apple",
	"policewoman": "bad apple",
	"Policewoman": "Bad apple",
	"police woman": "bad apple",
	"Police woman": "Bad apple",
	"policemen": "bad apples",
	"Policemen": "Bad apples",
	"police men": "bad apples",
	"Police men": "Bad apples",
	"policewomen": "bad apples",
	"Policewomen": "Bad apples",
	"police women": "bad apples",
	"Police women": "Bad apples",
	"police brutality": "bad apple brutality",
	"Police brutality": "Bad apple brutality",
	"police violence": "bad apple violence",
	"Police violence": "Bad apple violence",
	"police chief": "bad apple chief",
	"Police chief": "Bad apple chief",
	"police commissioner": "bad apple commissioner",
	"Police commissioner": "Bad apple commissioner",
	"police": "bad apples",
	"Police": "Bad apples",
	"law enforcement officer": "bad apple",
	"Law enforcement officer": "Bad apple",
	"law enforcement": "bad apples",
	"Law enforcement": "Bad apples",
	"cops": "bad apples",
	"Cops": "Bad apples",
	"cop": "bad apple",
	"Cop": "Bad apple"
}

function replaceTextInNode( node ){
	var withinContentEditable = !!( node.parentElement && node.parentElement.closest( "[contenteditable]" ) );
	var childOfTextArea = ( node.parentNode && node.parentNode.nodeName === "TEXTAREA" );
	var content;

	if( node.nodeType == Node.TEXT_NODE ){
		if( childOfTextArea || withinContentEditable ){
			return;
		}

		content = node.textContent;

		for( let replacement in replacements ){
			content = content.replace( new RegExp( `(\\W|^)${replacement}`, "gm" ), `$1${replacements[ replacement ]}` );
		}
	
		node.textContent = content;
	}
	else{
		for( let i = 0; i < node.childNodes.length; i++ ){
			replaceTextInNode( node.childNodes[ i ] );
		}
	}
}
  
replaceTextInNode( document.body );
  
var observer = new MutationObserver( ( mutations ) => {
	mutations.forEach( ( mutation ) => {
		if( mutation.addedNodes && mutation.addedNodes.length > 0 ){
			for( let i = 0; i < mutation.addedNodes.length; i++ ){
				replaceTextInNode( mutation.addedNodes[ i ] );
			}
		}
	} );
} );

observer.observe( document.body, {
	childList: true,
	subtree: true
} );